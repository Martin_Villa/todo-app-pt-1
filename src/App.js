// Got help from Suri and Tim La and worked with Latisha Johnson and Elly Hall

import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from "uuid";
uuidv4();
class App extends Component {
  state = {
    todos: todosList,
  };

  addTodo = (evt) => {
    if (evt.key === "Enter") {
      const newTodo = {
        userId: 1,
        id: uuidv4(),
        title: evt.target.value,
        completed: false,
      };

      this.setState({
        todos: [...this.state.todos, newTodo],
      });
      evt.target.value = "";
    }
  };
  onToggle = (id) => {
    let makingNewTodos = this.state.todos.map((todo) => {
      if (id === todo.id) {
        return { ...todo, completed: !todo.completed };
      }
      return { ...todo };
    });
    this.setState({
      todos: makingNewTodos,
    });
  };

  deleteTodo = (id) => {
    const filteredTodos = this.state.todos.filter((todo) => todo.id !== id);
    this.setState({
      todos: filteredTodos,
    });
  };
  handleAllDelete = () => {
    const filterItems = this.state.todos.filter(
      (todo) => todo.completed === false
    );
    this.setState({
      todos: filterItems,
    });
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            onKeyDown={this.addTodo}
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
          />
        </header>
        <TodoList
          todos={this.state.todos}
          onClick={this.onToggle}
          onChange={this.deleteTodo}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick={this.handleAllDelete} className="clear-completed">
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            onClick={(evt) => this.props.toggled(this.props.id)}
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
          />
          <label>{this.props.title}</label>
          <button
            onClick={(evt) => this.props.delete(this.props.id)}
            className="destroy"
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              key={todo.id}
              title={todo.title}
              toggled={this.props.onClick}
              id={todo.id}
              delete={this.props.onChange}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
